# mdnls

The goal of mdnls is to expand Sewell and Chen's 2016 work on latent spaces for dynamic networks to networks with weighted and directed edges, and incorporate the ability to handle multilayer networks.

## Example

``` r
test <- mdnls(network, p, options = list(burn = 100, samples = 5000))
```
