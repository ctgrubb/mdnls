#' Internal - Sample Sigma
#'
#' Samples Sigma from the full conditional posterior distribution
#'
#' @param positions position matrix
#' @param p dimensionality
#' @param tau_prior prior on tau (initial guess)
#' @param n_nodes number of nodes
#' @param n_times number of time points
#' @param delta tunable epsilon > 0
#'
#' @importFrom MCMCpack rinvgamma
#'
#' @return new sample for Sigma
mdnls_samplesigma <- function(positions, p, sigma_prior, n_nodes, n_times, delta) {
  normdsum <- 0
  for(i in 2:n_times) {
    normdsum <- normdsum + sum(apply(positions[(25*(i-1) + 1:n_nodes), ] - positions[(25*(i-2) + 1:n_nodes), ], 1, function(x) sum(x^2)))
  }
  sigma <- rinvgamma(1, 2 + delta + n_nodes * p * (n_times - 1) / 2, (1 + delta) * sigma_prior + normdsum / 2)
  return(sigma)
}
